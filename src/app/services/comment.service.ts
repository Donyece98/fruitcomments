import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable, of
} from 'rxjs';
import {Comment} from './../entities/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private comments: Comment[]=[];
  private lastAuthorName: BehaviorSubject<string>;

  constructor() { 
    this.lastAuthorName = 
        new BehaviorSubject<string>('No Author as yet');
  }

  /**
   * @param comment Comment
   * @return Observable<boolean> success
   */
  public add(comment: Comment):Observable<boolean>{
    comment.id = this.comments.length+1; //generate id
    this.comments.push(comment); // add comment to array
    //add author name to subject
    this.lastAuthorName.next(comment.author);
    //of function returns an observable of the value
    // passed to it , i.e. Observable value which is true
    return of(true);
  }
  getAll(): Observable<Comment[]>{
    return of(this.comments);
  }
  getLastAuthorName():Observable<string>{
    return this.lastAuthorName;
  }
}
